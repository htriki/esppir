
#include <ESP8266WiFi.h>

const char* ssid = "xxxxxx";           // nom du wifi
const char* password = "xxxxxxxx";     // mot de passe wifi


int ledPin = 4;             // LED qui s'allume si détection
int inputPin = 16;          // capteur infrarouge PIR
int pirState = LOW;         // pas encore de mouvement détecté au départ
int val = 0;                // Lecture du statut

int calibrationTime = 20;   // calibrage du capteur PIR

WiFiServer server(80);
  
    void setup() {
      
        pinMode(ledPin, OUTPUT);      //  LED en sortie
        pinMode(inputPin, INPUT);     // Détecteur PIR en entrée
        Serial.begin(115200);           //Affichage série

                Serial.println();
                Serial.println();
                Serial.print("Connecting to ");
                Serial.println(ssid);
              
                // Connection au réseau
                WiFi.begin(ssid, password);
               
                while (WiFi.status() != WL_CONNECTED) {
                  
                    delay(500);
                    Serial.print("no.connect");
                }
                Serial.println("");
                Serial.println("WiFi connected");
               
                // Start the server
                server.begin();
                Serial.println("Server started");
               
                // Print the IP address
                Serial.print("Use this URL to connect: ");
                Serial.print("http://");
                Serial.print(WiFi.localIP());
                Serial.println("/");


    
        Serial.println("...Calibrage ESP...");
        for(int i = 0; i < calibrationTime; i++) {
          Serial.print(i);
          delay(1000);
        }

    
    }

    void loop() {

              WiFiClient client = server.available();
              if (!client) {
                return;
              }
         
              // Wait until the client sends some data
              Serial.println("new client");
              while(!client.available()){
                delay(1);
              }

      
      val = digitalRead(inputPin);                // Lire le statut
      
      Serial.print( val );
     
      if (val == HIGH) {                         
        
        //digitalWrite(ledPin, HIGH);               // Allumer la LED
        
        if (pirState == LOW) {                   
          
          Serial.println("- Mouvement detecte ");
          pirState = HIGH;            
          
        }
       
      } else {                        
      
          //digitalWrite(ledPin, LOW);              // éteindre la ELd
          
          if (pirState == HIGH) {     
            
            Serial.println("- stop");
            pirState = LOW;           
            
          }
      }


          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("");
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<head>");
          client.println("<META HTTP-EQUIV='Refresh' CONTENT='2; URL=http://192.168.0.22/'>"); 
          client.println("<meta name='apple-mobile-web-app-capable' content='yes' />");
          client.println("<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />");
          client.println("</head>");
          client.println("<body bgcolor = \"#f7e6ec\">"); 
          client.println("<hr/>");
          client.println("<h4><center> Hocine </center></h4>");
          client.println("<hr/>");
          client.println("<center>");
          client.println("<br/><br/>");

          client.println( val );
          
          if (val == HIGH) {
            
            client.println( "<br/> Détection" );
          } else {
            client.println( "<br/> Rien................." );  
          }
      
          client.println("<br/><br/>");
          client.println("</center>");
          client.println("</body>");
          client.println("</html>"); 
          delay(1);
    
    }


